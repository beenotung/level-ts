import { resolve, isAbsolute } from 'path';
import { Serializer, isSerializableClass, Serializable } from 'honeybuf';

// tslint:disable: jsdoc-format
// tslint:disable-next-line: no-var-requires
const leveldown = require('leveldown');
// tslint:disable-next-line: no-var-requires
const levelup = require('levelup');

const instances: {
  [fullpath: string]: any;
} = {};

interface IChainObject<DefaultType> {
  del(key: string): IChainObject<DefaultType>;
  get(key: string): IChainObject<DefaultType>;
  put<EntryType = DefaultType>(key: string, value: EntryType): IChainObject<EntryType>;
  finish(): Promise<DefaultType[]>;
}

interface IConstructor<T> {
  new(...args: any): T;
  [key: string]: any;
}

enum SerializationType {
  JSON = '{'.charCodeAt(0), // Innate
  Honeybuf = 0x1,
}

// see https://github.com/Level/packager/blob/master/level-packager.js
function level(ldown: any) {
  // tslint:disable-next-line: ban-types
  function WrappedLevel(location: string | Function, options?: any, callback?: Function) {
    if (typeof location === 'function') {
      callback = location;
    } else if (typeof options === 'function') {
      callback = options;
    }

    if (!isObject(options)) {
      options = isObject(location) ? location : {};
    }

    return levelup(ldown(location, options), options, callback);
  }

  function isObject(o: any) {
    return typeof o === 'object' && o !== null;
  }

  ['destroy', 'repair'].forEach((m) => {
    if (typeof leveldown[m] === 'function') {
      (WrappedLevel as any)[m] = () => {
        leveldown[m].apply(leveldown, arguments);
      };
    }
  });

  WrappedLevel.errors = levelup.errors;

  return WrappedLevel;
}

export default class Level<DefaultType = any> {

  public get chain(): IChainObject<DefaultType> {
    // tslint:disable-next-line: no-this-assignment
    const instance = this;
    const promises: Array<Promise<any>> = [];
    return {
      get(key: string) { promises.push(instance.get(key)); return this; },
      del(key: string) { promises.push(instance.del(key)); return this; },
      put(key: string, value: any) { promises.push(instance.put(key, value)); return this as any; },
      async finish() { return (await Promise.all(promises)).filter((v) => !!v); },
    };
  }
  public static rootFolder = process.env.DATABASES || process.env.DATABASES_ROOT || process.cwd();
  public static setRoot(path: string) {
    this.rootFolder = path;
  }

  private DB: any;
  private defaultType?: IConstructor<any>;
  private defaultSerializer?: Serializer<DefaultType>;

  constructor(database: object)
  // tslint:disable-next-line: unified-signatures
  constructor(path: string)
  constructor(argument: string | any, defaultType?: IConstructor<DefaultType>) {
    if (defaultType && isSerializableClass(defaultType)) {
      this.defaultType = defaultType;
      this.defaultSerializer = new Serializer(this.defaultType);
    }

    if (typeof argument === 'string') {
      const fullpath = isAbsolute(argument) ? argument : resolve(Level.rootFolder, argument);
      this.DB = instances[fullpath]
        ? instances[fullpath]
        : instances[fullpath] = level(leveldown)(fullpath);
    } else if (argument instanceof levelup) {
      this.DB = argument;
    } else {
      throw new Error('No valid database instance or path provided');
    }
  }

  public async find<EntryType = DefaultType>(func: (value: EntryType, ind: number, all: EntryType[]) => boolean | null | undefined, honeyType?: IConstructor<EntryType>): Promise<EntryType | undefined> {
    const all = await this.all<EntryType>(honeyType);
    return all.find(func as any);
  }

  public async filter<EntryType = DefaultType>(func: (value: EntryType, ind: number, all: EntryType[]) => boolean | null | undefined, honeyType?: IConstructor<EntryType>) {
    const all = await this.all<EntryType>(honeyType);
    return all.filter(func);
  }

  public exists(key: string): Promise<boolean> {
    return new Promise((res, rej) => {
      this.DB.get(key)
        .catch((e: any) => e.notFound ? res(false) : rej(e))
        .then(() => res(true));
    });
  }

  public async get<EntryType = DefaultType>(key: string, honeyType?: IConstructor<EntryType>): Promise<EntryType | Buffer> {
    const buf: Buffer = await this.DB.get(key);
    const obj = this.deserialize(buf, honeyType);
    return obj;
  }

  public async put<EntryType = DefaultType>(key: string, value: EntryType, honeyType?: IConstructor<EntryType>): Promise<EntryType> {
    await this.DB.put(key, this.serialize(value, honeyType));
    return value;
  }

  public async del(key: string): Promise<void> {
    await this.DB.del(key);
  }

  public async merge<EntryType = DefaultType>(key: string, config: Partial<EntryType>, honeyType?: IConstructor<EntryType>): Promise<EntryType | Buffer> {
    const oldConfig = await this.get<EntryType>(key, honeyType);
    if (!(oldConfig instanceof Buffer)) {
      const newConfig = oldConfig;
      const keys = Object.getOwnPropertyNames(oldConfig);
      for (const propkey of keys) {
        (newConfig as any)[propkey] = (oldConfig as any)[propkey];
      }
      await this.put<EntryType>(key, newConfig, honeyType);
      return newConfig;
    } else {
      await this.put(key, config, honeyType);
      return config as any;
    }
  }

  public async all<EntryType = DefaultType>(honeyType?: IConstructor<EntryType>): Promise<EntryType[]> {
    return this.stream<EntryType>({ keys: false }, honeyType);
  }

  public stream<EntryType = DefaultType>(opts: Partial<IStreamOptions> & { keys?: true; values: false }, honeyType?: IConstructor<EntryType>): Promise<Buffer[]>;
  public stream<EntryType = DefaultType>(opts: Partial<IStreamOptions> & { keys: false; values?: true }, honeyType?: IConstructor<EntryType>): Promise<EntryType[]>;
  public stream<EntryType = DefaultType>(opts: Partial<IStreamOptions> & { keys?: true; values?: true }, honeyType?: IConstructor<EntryType>): Promise<Array<{ key: Buffer; value: EntryType }>>;
  public stream<EntryType = DefaultType>(opts: Partial<IStreamOptions>, honeyType?: IConstructor<EntryType>): Promise<any[]> {
    return new Promise((resolver, reject) => {
      const returnArray: any[] = [];
      if (opts.all) Object.assign(opts, { gte: opts.all, lte: opts.all + '\xff' });
      this.DB
        .createReadStream(opts)
        .on('data', (data: any) => {
          if (opts.values !== false && opts.keys !== false) data.value = this.deserialize(data.value, honeyType);
          if (opts.keys === false) data = this.deserialize(data, honeyType);
          returnArray.push(data);
        })
        .on('error', reject)
        .on('end', () => resolver(returnArray));
    });
  }

  public iterate<EntryType = DefaultType>(opts: Partial<IStreamOptions> & { keys?: true, values: false }): IStream<string>;
  public iterate<EntryType = DefaultType>(opts: Partial<IStreamOptions> & { keys: false, values?: true }): IStream<EntryType>;
  public iterate<EntryType = DefaultType>(opts?: Partial<IStreamOptions> & { keys?: true, values?: true }): IStream<{ key: string, value: EntryType }>;
  public iterate<EntryType = DefaultType>(optionalOpts?: Partial<IStreamOptions> & { keys?: boolean, values?: boolean }): IStream<any> {
    const opts = optionalOpts || {};
    if (opts.all) {
      Object.assign(opts, { gte: opts.all, lte: opts.all + '\xff' });
    }
    let resolveEnd!: () => void;
    let rejectEnd!: (error?: any) => void;
    const endPromise = new Promise<void>((resolver, reject) => {
      resolveEnd = resolver;
      rejectEnd = reject;
    });
    const dataCbs: Array<(data: any) => void> = [];
    const stream: IStream<any> = {
          onData(cb: (data: any) => void) {
            dataCbs.push(cb);
          },
          wait(): Promise<void> {
            return endPromise;
          },
        };
    this.DB
        .createReadStream(opts)
        .on('data', (data: any) => {
          if (opts.values !== false && opts.keys !== false) {
            data.value = JSON.parse(data.value);
          }
          if (opts.keys === false) {
            data = JSON.parse(data);
          }
          dataCbs.forEach((cb) => cb(data));
        })
        .on('error', rejectEnd)
        .on('end', resolveEnd);
    return stream;
  }

  private deserialize<EntryType = DefaultType>(buf: Buffer, honeyType?: IConstructor<EntryType>): EntryType | Buffer {
    if (buf[0] === SerializationType.Honeybuf) {
      let ser: Serializer<EntryType> | undefined = this.defaultSerializer as any;
      if (honeyType) {
        ser = new Serializer(honeyType);
      }
      if (!ser) throw new Error('Cannot deserialize without default or specified honeyType');
      return ser.Deserialize(buf);
    } else if (buf[0] === SerializationType.JSON) {
      return JSON.parse(buf.toString('utf-8'));
    }
    return buf;
  }

  private addPrefix(buf: Buffer, type: SerializationType) {
    return Buffer.concat([Buffer.from([type]), buf]);
  }
  private serialize<EntryType = DefaultType>(value: EntryType, honeyType?: IConstructor<EntryType>): Buffer {
    if (honeyType && isSerializableClass(honeyType)) {
      const ser = new Serializer(honeyType);
      return this.addPrefix(ser.Serialize(value), SerializationType.Honeybuf);
    } else if (this.defaultSerializer && this.defaultType && value instanceof (this.defaultType as any)) {
      return this.addPrefix(this.defaultSerializer.Serialize(value as any), SerializationType.Honeybuf);
    } else {
      try {
        return Buffer.from(value as any);
      } catch {
        return Buffer.from(JSON.stringify(value));
      }
    }
  }
}

interface IStreamOptions {
  /**define the lower bound of the range to be streamed. Only entries where the key is greater than (or equal to) this option will be included in the range. When reverse=true the order will be reversed, but the entries streamed will be the same. */
  gt: string;
  gte: string;
  /**define the higher bound of the range to be streamed. Only entries where the key is less than (or equal to) this option will be included in the range. When reverse=true the order will be reversed, but the entries streamed will be the same. */
  lt: string;
  lte: string;
  /**Using gte and lte with this value */
  all: string;

  /**(default: false) stream entries in reverse order. Beware that due to the way that stores like LevelDB work, a reverse seek can be slower than a forward seek. */
  reverse: boolean;

  // tslint:disable-next-line: max-line-length
  /**(default: -1) limit the number of entries collected by this stream. This number represents a maximum number of entries and may not be reached if you get to the end of the range first. A value of -1 means there is no limit. When reverse=true the entries with the highest keys will be returned instead of the lowest keys. */
  limit: number;
  /**(default: true) whether the results should contain keys. If set to true and values set to false then results will simply be keys, rather than objects with a key property. Used internally by the createKeyStream() method. */
  keys: boolean;
  /**(default: true) whether the results should contain values. If set to true and keys set to false then results will simply be values, rather than objects with a value property. Used internally by the createValueStream() method. */
  values: boolean;
}

interface IStream<T> {
  onData(cb: (data: T) => void): void;

  wait(): Promise<void>; // resolve when ended
}
