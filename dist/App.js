"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = require("path");
const honeybuf_1 = require("honeybuf");
// tslint:disable: jsdoc-format
// tslint:disable-next-line: no-var-requires
const leveldown = require('leveldown');
// tslint:disable-next-line: no-var-requires
const levelup = require('levelup');
const instances = {};
var SerializationType;
(function (SerializationType) {
    SerializationType[SerializationType["JSON"] = '{'.charCodeAt(0)] = "JSON";
    SerializationType[SerializationType["Honeybuf"] = 1] = "Honeybuf";
})(SerializationType || (SerializationType = {}));
// see https://github.com/Level/packager/blob/master/level-packager.js
function level(ldown) {
    // tslint:disable-next-line: ban-types
    function WrappedLevel(location, options, callback) {
        if (typeof location === 'function') {
            callback = location;
        }
        else if (typeof options === 'function') {
            callback = options;
        }
        if (!isObject(options)) {
            options = isObject(location) ? location : {};
        }
        return levelup(ldown(location, options), options, callback);
    }
    function isObject(o) {
        return typeof o === 'object' && o !== null;
    }
    ['destroy', 'repair'].forEach((m) => {
        if (typeof leveldown[m] === 'function') {
            WrappedLevel[m] = () => {
                leveldown[m].apply(leveldown, arguments);
            };
        }
    });
    WrappedLevel.errors = levelup.errors;
    return WrappedLevel;
}
class Level {
    constructor(argument, defaultType) {
        if (defaultType && honeybuf_1.isSerializableClass(defaultType)) {
            this.defaultType = defaultType;
            this.defaultSerializer = new honeybuf_1.Serializer(this.defaultType);
        }
        if (typeof argument === 'string') {
            const fullpath = path_1.isAbsolute(argument) ? argument : path_1.resolve(Level.rootFolder, argument);
            this.DB = instances[fullpath]
                ? instances[fullpath]
                : instances[fullpath] = level(leveldown)(fullpath);
        }
        else if (argument instanceof levelup) {
            this.DB = argument;
        }
        else {
            throw new Error('No valid database instance or path provided');
        }
    }
    get chain() {
        // tslint:disable-next-line: no-this-assignment
        const instance = this;
        const promises = [];
        return {
            get(key) { promises.push(instance.get(key)); return this; },
            del(key) { promises.push(instance.del(key)); return this; },
            put(key, value) { promises.push(instance.put(key, value)); return this; },
            async finish() { return (await Promise.all(promises)).filter((v) => !!v); },
        };
    }
    static setRoot(path) {
        this.rootFolder = path;
    }
    async find(func, honeyType) {
        const all = await this.all(honeyType);
        return all.find(func);
    }
    async filter(func, honeyType) {
        const all = await this.all(honeyType);
        return all.filter(func);
    }
    exists(key) {
        return new Promise((res, rej) => {
            this.DB.get(key)
                .catch((e) => e.notFound ? res(false) : rej(e))
                .then(() => res(true));
        });
    }
    async get(key, honeyType) {
        const buf = await this.DB.get(key);
        const obj = this.deserialize(buf, honeyType);
        return obj;
    }
    async put(key, value, honeyType) {
        await this.DB.put(key, this.serialize(value, honeyType));
        return value;
    }
    async del(key) {
        await this.DB.del(key);
    }
    async merge(key, config, honeyType) {
        const oldConfig = await this.get(key, honeyType);
        if (!(oldConfig instanceof Buffer)) {
            const newConfig = oldConfig;
            const keys = Object.getOwnPropertyNames(oldConfig);
            for (const propkey of keys) {
                newConfig[propkey] = oldConfig[propkey];
            }
            await this.put(key, newConfig, honeyType);
            return newConfig;
        }
        else {
            await this.put(key, config, honeyType);
            return config;
        }
    }
    async all(honeyType) {
        return this.stream({ keys: false }, honeyType);
    }
    stream(opts, honeyType) {
        return new Promise((resolver, reject) => {
            const returnArray = [];
            if (opts.all)
                Object.assign(opts, { gte: opts.all, lte: opts.all + '\xff' });
            this.DB
                .createReadStream(opts)
                .on('data', (data) => {
                if (opts.values !== false && opts.keys !== false)
                    data.value = this.deserialize(data.value, honeyType);
                if (opts.keys === false)
                    data = this.deserialize(data, honeyType);
                returnArray.push(data);
            })
                .on('error', reject)
                .on('end', () => resolver(returnArray));
        });
    }
    iterate(optionalOpts) {
        const opts = optionalOpts || {};
        if (opts.all) {
            Object.assign(opts, { gte: opts.all, lte: opts.all + '\xff' });
        }
        let resolveEnd;
        let rejectEnd;
        const endPromise = new Promise((resolver, reject) => {
            resolveEnd = resolver;
            rejectEnd = reject;
        });
        const dataCbs = [];
        const stream = {
            onData(cb) {
                dataCbs.push(cb);
            },
            wait() {
                return endPromise;
            },
        };
        this.DB
            .createReadStream(opts)
            .on('data', (data) => {
            if (opts.values !== false && opts.keys !== false) {
                data.value = JSON.parse(data.value);
            }
            if (opts.keys === false) {
                data = JSON.parse(data);
            }
            dataCbs.forEach((cb) => cb(data));
        })
            .on('error', rejectEnd)
            .on('end', resolveEnd);
        return stream;
    }
    deserialize(buf, honeyType) {
        if (buf[0] === SerializationType.Honeybuf) {
            let ser = this.defaultSerializer;
            if (honeyType) {
                ser = new honeybuf_1.Serializer(honeyType);
            }
            if (!ser)
                throw new Error('Cannot deserialize without default or specified honeyType');
            return ser.Deserialize(buf);
        }
        else if (buf[0] === SerializationType.JSON) {
            return JSON.parse(buf.toString('utf-8'));
        }
        return buf;
    }
    addPrefix(buf, type) {
        return Buffer.concat([Buffer.from([type]), buf]);
    }
    serialize(value, honeyType) {
        if (honeyType && honeybuf_1.isSerializableClass(honeyType)) {
            const ser = new honeybuf_1.Serializer(honeyType);
            return this.addPrefix(ser.Serialize(value), SerializationType.Honeybuf);
        }
        else if (this.defaultSerializer && this.defaultType && value instanceof this.defaultType) {
            return this.addPrefix(this.defaultSerializer.Serialize(value), SerializationType.Honeybuf);
        }
        else {
            try {
                return Buffer.from(value);
            }
            catch {
                return Buffer.from(JSON.stringify(value));
            }
        }
    }
}
Level.rootFolder = process.env.DATABASES || process.env.DATABASES_ROOT || process.cwd();
exports.default = Level;
//# sourceMappingURL=App.js.map