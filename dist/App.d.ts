/// <reference types="node" />
interface IChainObject<DefaultType> {
    del(key: string): IChainObject<DefaultType>;
    get(key: string): IChainObject<DefaultType>;
    put<EntryType = DefaultType>(key: string, value: EntryType): IChainObject<EntryType>;
    finish(): Promise<DefaultType[]>;
}
interface IConstructor<T> {
    new (...args: any): T;
    [key: string]: any;
}
export default class Level<DefaultType = any> {
    readonly chain: IChainObject<DefaultType>;
    static rootFolder: string;
    static setRoot(path: string): void;
    private DB;
    private defaultType?;
    private defaultSerializer?;
    constructor(database: object);
    constructor(path: string);
    find<EntryType = DefaultType>(func: (value: EntryType, ind: number, all: EntryType[]) => boolean | null | undefined, honeyType?: IConstructor<EntryType>): Promise<EntryType | undefined>;
    filter<EntryType = DefaultType>(func: (value: EntryType, ind: number, all: EntryType[]) => boolean | null | undefined, honeyType?: IConstructor<EntryType>): Promise<EntryType[]>;
    exists(key: string): Promise<boolean>;
    get<EntryType = DefaultType>(key: string, honeyType?: IConstructor<EntryType>): Promise<EntryType | Buffer>;
    put<EntryType = DefaultType>(key: string, value: EntryType, honeyType?: IConstructor<EntryType>): Promise<EntryType>;
    del(key: string): Promise<void>;
    merge<EntryType = DefaultType>(key: string, config: Partial<EntryType>, honeyType?: IConstructor<EntryType>): Promise<EntryType | Buffer>;
    all<EntryType = DefaultType>(honeyType?: IConstructor<EntryType>): Promise<EntryType[]>;
    stream<EntryType = DefaultType>(opts: Partial<IStreamOptions> & {
        keys?: true;
        values: false;
    }, honeyType?: IConstructor<EntryType>): Promise<Buffer[]>;
    stream<EntryType = DefaultType>(opts: Partial<IStreamOptions> & {
        keys: false;
        values?: true;
    }, honeyType?: IConstructor<EntryType>): Promise<EntryType[]>;
    stream<EntryType = DefaultType>(opts: Partial<IStreamOptions> & {
        keys?: true;
        values?: true;
    }, honeyType?: IConstructor<EntryType>): Promise<Array<{
        key: Buffer;
        value: EntryType;
    }>>;
    iterate<EntryType = DefaultType>(opts: Partial<IStreamOptions> & {
        keys?: true;
        values: false;
    }): IStream<string>;
    iterate<EntryType = DefaultType>(opts: Partial<IStreamOptions> & {
        keys: false;
        values?: true;
    }): IStream<EntryType>;
    iterate<EntryType = DefaultType>(opts?: Partial<IStreamOptions> & {
        keys?: true;
        values?: true;
    }): IStream<{
        key: string;
        value: EntryType;
    }>;
    private deserialize;
    private addPrefix;
    private serialize;
}
interface IStreamOptions {
    /**define the lower bound of the range to be streamed. Only entries where the key is greater than (or equal to) this option will be included in the range. When reverse=true the order will be reversed, but the entries streamed will be the same. */
    gt: string;
    gte: string;
    /**define the higher bound of the range to be streamed. Only entries where the key is less than (or equal to) this option will be included in the range. When reverse=true the order will be reversed, but the entries streamed will be the same. */
    lt: string;
    lte: string;
    /**Using gte and lte with this value */
    all: string;
    /**(default: false) stream entries in reverse order. Beware that due to the way that stores like LevelDB work, a reverse seek can be slower than a forward seek. */
    reverse: boolean;
    /**(default: -1) limit the number of entries collected by this stream. This number represents a maximum number of entries and may not be reached if you get to the end of the range first. A value of -1 means there is no limit. When reverse=true the entries with the highest keys will be returned instead of the lowest keys. */
    limit: number;
    /**(default: true) whether the results should contain keys. If set to true and values set to false then results will simply be keys, rather than objects with a key property. Used internally by the createKeyStream() method. */
    keys: boolean;
    /**(default: true) whether the results should contain values. If set to true and keys set to false then results will simply be values, rather than objects with a value property. Used internally by the createValueStream() method. */
    values: boolean;
}
interface IStream<T> {
    onData(cb: (data: T) => void): void;
    wait(): Promise<void>;
}
export {};
//# sourceMappingURL=App.d.ts.map