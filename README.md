# level-honeybuf (based on level-ts)
A leveldb force promises, class, JSON and typescript implementing wrapper.

Changes made in this module compared to `level-ts`:
* Added support for honeybuf serialization
* string -> Buffer (no encoding-down)
* Fallback (in order): honeybuf -> JSON -> Buffer
* Added support to iterate dataset in streaming (previously can only retrieve in batch)

For original docs look at: https://gitlab.com/NotRyan_/level-ts/tree/master/dist